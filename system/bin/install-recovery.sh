#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/recovery:10307840:3ef16e00efcd36b51246e4717e89140597520e29; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/boot:8438016:17c14f50d5bfb308d319fd595bdd3fdfffd7e153 EMMC:/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/recovery 3ef16e00efcd36b51246e4717e89140597520e29 10307840 17c14f50d5bfb308d319fd595bdd3fdfffd7e153:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
